package com.epam.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.*;

public class BlockingQueueTask {

    private static Logger log = LogManager.getLogger(BlockingQueueTask.class);
    private BlockingQueue<String> blockingQueue;
    private boolean flag = true;

    public BlockingQueueTask() {
        blockingQueue = new PriorityBlockingQueue<>();
    }

    public void doTask() {
        ExecutorService executorService = Executors.newFixedThreadPool(3);
        executorService.execute(this::writeToQueue);
        executorService.execute(this::readFromQueue);
        executorService.shutdown();
    }

    private void writeToQueue() {
        try {
            for (int i = 1; i <= 5; i++) {
                blockingQueue.put("data written... - " + i);
                Thread.sleep(2000);
            }
            flag = false;
            blockingQueue.put("Done");
        } catch (InterruptedException e) {
            log.error(e.getMessage());
        }
    }

    private void readFromQueue() {
        try {
            while(flag){
                log.info(blockingQueue.take());
            }
        } catch (InterruptedException e) {
            log.error(e.getMessage());
        }
    }
}
