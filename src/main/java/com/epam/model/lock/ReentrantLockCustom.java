package com.epam.model.lock;

public class ReentrantLockCustom implements LockCustom {

    private int lockCount;
    private long idThreadLock;

    public ReentrantLockCustom() {
        lockCount = 0;
    }

    @Override
    public synchronized void lock() {
        if (lockCount == 0){
            lockCount++;
            idThreadLock = Thread.currentThread().getId();
        }else if (lockCount > 0
                && idThreadLock == Thread.currentThread().getId()){
            lockCount++;
        }else{
            try {
                wait();
                lockCount++;
                idThreadLock = Thread.currentThread().getId();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public synchronized void unlock() {
        if (lockCount == 0){
            throw new IllegalMonitorStateException();
        }
        lockCount--;
        if (lockCount == 0){
            notify();
        }
    }

    @Override
    public synchronized boolean tryLock() {
        if (lockCount == 0){
            lock();
            return true;
        }else{
            return false;
        }
    }
}
