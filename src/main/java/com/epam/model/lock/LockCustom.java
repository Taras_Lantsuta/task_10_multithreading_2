package com.epam.model.lock;

public interface LockCustom {

    void lock();

    void unlock();

    boolean tryLock();
}
